# z-push Openshift container

This is a wrapper to build an Openshift image of Z-Push.

## How it works

We take:

* The official Z-Push git repo in the form of a submodule
* A `s2i` builder from Software Collections providing a PHP 7.3 environment, PHP composer etc
* A Dockerfile that adds some customisations to the builder
* Config files hosted on Openshift

and we produce an image that is deployed on Openshift.

## Custom Builder in detail

We need to add some missing packages to the default builder so we follow the procedure at
[KB0004498](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004498)

The custom builder is using the top-level Docker file to prepare the image, including custom patches on top of the Z-Push code.

We use the official [Software Collections s2i builder for PHP 7.3](https://github.com/sclorg/s2i-php-container/tree/master/7.3).

## Get the SSH deploy key used by the Openshift Project:

Run:
```
oc get secrets sshdeploykey -o yaml
```

The public key is in the metadata annotations, for `cern.ch/ssh-public-key`

For a new project, you need to go to our gitlab project and add it:
- Add the key on https://gitlab.cern.ch/mail/zpush-openshift/-/settings/repository
   - Expend `Deploy Keys`
   - Title should be `${site}@openshift`
   - Value should be the public key found above
   - Write access should not be allowed
   - Don't forget to click on `Add key`
- Enable the key on https://gitlab.cern.ch/mail/z-push/-/settings/repository:
   - Expend `Deploy Keys`
   - Click on `Privately accessible deploy keys` below the `Add key` button
   - Find the key you just added, it should be at the bottom
   - Click on `Enable`

## Create the custom builder (only needed once per Openshift project)

This is using the top-level `Dockerfile`.

For `test`:
```
oc new-build \
  --docker-image=centos/php-73-centos7 --name='zpush-builder' \
  --strategy=docker --source-secret='sshdeploykey' \
  'ssh://git@gitlab.cern.ch:7999/mail/zpush-openshift.git#test'
```

For `master`:
```
oc new-build \
  --docker-image=centos/php-73-centos7 --name='zpush-builder' \
  --strategy=docker --source-secret='sshdeploykey' \
  'ssh://git@gitlab.cern.ch:7999/mail/zpush-openshift.git#master'
```

## Clean up default webhooks and set gitlab webhook

- Delete the automatically generated webhooks with:
   - `oc set triggers bc 'zpush-builder' --from-webhook --remove=true`
   - `oc set triggers bc 'zpush-builder' --from-github --remove=true`
- Generate a random hex string, with e.g.:
  ```hexdump -n 8 -e '4/4 "%08x" 1 "\n"' /dev/random```
- Create a secret named 'gitlab-webhook' with that value with:
  ```
  oc create secret generic gitlab-webhook --type=string \
  --from-literal=WebHookSecretKey=<inserthexhere>
  ```
- Create the gitlab webhook with:
  ```
  oc patch bc/zpush-builder -p '{"spec": {"triggers": [{"type": "GitLab", \
  "gitlab": { "secretReference": {"name": "gitlab-webhook" }}}]}}'
  ```
- Get the webhook link via:
  ```oc describe bc zpush-builder```
- Enable that gitlab hook on https://gitlab.cern.ch/mail/zpush-openshift/hooks:
  - Set the URL to the 'URL: ' value from the output of the command above, after
    replacing `<secret>` with the hex string generated above
  - Set the `Secret Token` to the hex string generated above
  - Under `Push events`, insert the name of the branch to follow (e.g. `master`
    or `test`
  - Click on `Add Webhook`

## Create an app based on the custom builder (Only needed once per Openshift project)

For `test`:
```
oc new-app \
  'zpush-builder~ssh://git@gitlab.cern.ch:7999/mail/zpush-openshift.git#test' \
  --source-secret='sshdeploykey' --name zpush-test
```

For `master`:
```
oc new-app \
  'zpush-builder~ssh://git@gitlab.cern.ch:7999/mail/zpush-openshift.git#master' \
  --source-secret='sshdeploykey' --name zpush
```

## Create the config maps

<TBA>

## Create the different mounts/shares

In the following commands, replace <name> by:
   - For `test`: `zpush-test`
   - For `master`: `zpush`

- Create empty dirs for logs:
  ```oc set volumes dc/<name> --add=true --name=zpush-log -m /var/log/z-push/```
- HTTP configuration:
  ````
  oc set volumes dc/<name> --add=true --name=httpd-config -t configmap --default-mode 422 \
    --configmap-name='httpd-config' --sub-path='z-push.conf' -m /etc/httpd/conf.d/z-push.conf
  ```
- Z-push configuration
  - Add volume:
  ```
  oc patch dc/<name> -p '{"spec": { "template": { "spec": {"volumes": [{"name": \
  "zpush-config", "configMap": {"defaultMode": 422, "name": "zpush-config"}}]}}}}'
  ```
  - Add mounts (be careful of the two <name> in the middle):
  ```
    for x in z-push.conf.php:/opt/app-root/src/config.php \
             caldav.conf.php:/opt/app-root/src/backend/caldav/config.php \
             carddav.conf.php:/opt/app-root/src/backend/carddav/config.php \
             combined.conf.php:/opt/app-root/src/backend/combined/config.php \
             galsearch-ldap.conf.php:/opt/app-root/src/backend/searchldap/config.php \
             imap.conf.php:/opt/app-root/src/backend/imap/config.php \
             state-sql.conf.php:/opt/app-root/src/backend/sqlstatemachine/config.php \
             policies.ini:/opt/app-root/src/policies.ini; do \
        oc patch dc/<name> -p "{\"spec\": { \"template\": { \"spec\": { \
             \"containers\": [{\"name\": \"<name>\", \"volumeMounts\": \
             [{\"name\": \"zpush-config\", \"mountPath\": \"${x/*:/}\", \
             \"subPath\": \"${x/:*/}\"}]}]}}}}"; \
    done
   ```

## Create route from the outside

- Delete silly port 8443 from service with:
  - For `test`: ```oc patch svc/zpush-test --type json -p '[{"op": "remove", "path":"/spec/ports/1"}]'```
  - For `master`: ```oc patch svc/zpush --type json -p '[{"op": "remove", "path":"/spec/ports/1"}]'```
- Delete silly port 8443 from deployment config with:
  - For `test`: ```oc patch dc/zpush-test --type json  -p '[{"op": "remove", "path":"/spec/template/spec/containers/0/ports/1"}]'```
  - For `master`: ```oc patch dc/zpush --type json  -p '[{"op": "remove", "path":"/spec/template/spec/containers/0/ports/1"}]'```
- Create the route with:
  - For `tesŧ`: ```oc create route edge --insecure-policy=Redirect --service='svc/zpush-test'```
  - For `master`: ```oc create route edge --insecure-policy=Redirect --service='svc/zpush'```

## Route visibility from the intranet/internet

- Expose the route to the internet (for direct access to e.g. preprod):
  - `oc annotate route/zpush 'router.cern.ch/network-visibility=Internet'`

- Restrict the route to the intranet:
  - `oc annotate route/zpush 'router.cern.ch/network-visibility=Intranet'`


## Re-build the custom builder

```
oc start-build zpush-builder
oc logs -f bc/zpush-builder
```
NOTE: if successful this will trigger a re-build of the app image!

## Re-build the app image

```
oc start-build zpush
oc logs -f bc/zpush
```
