# A FROM line must be present but is ignored. It will be overridden by the --image-stream parameter in the buildConfig
FROM centos/php-73-centos7

# Temporarily switch to root user to install packages
USER root

# We need the z-push repo for php-awl and the cern repo for CERN-CA-certs
ADD etc/yum.repos.d/*.repo /etc/yum.repos.d/
ADD etc/pki/rpm-gpg/RPM-GPG-KEY-cern /etc/pki/rpm-gpg/

# Install and CERN-CA-certs epel for PHP deps
RUN yum install -y \
  epel-release CERN-CA-certs && \
  yum clean all

# Install PHP dependencies
RUN yum install -y \
  rh-php73-php-imap \
  rh-php73-php-xsl \
  php-awl && \
  yum clean all

# Fixes for Apache
RUN yum remove -y \
  httpd24-mod_ssl
RUN sed -ri \
  -e 's!Directory\ "/opt/app-root/src"!Directory\ "/var/www"!' \
  /etc/httpd/conf/httpd.conf
RUN rm /etc/httpd/conf.d/welcome.conf \
  /etc/httpd/conf.d/userdir.conf

# Add Z-Push patches
ADD patches/* /opt/app-root/src/

# Make sure the final image runs as unprivileged user
USER 1001
